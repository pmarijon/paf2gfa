import sys
import logging
import argparse

from paf2gfa import Parser

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(prog="paf2gfa",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument('paf', type=argparse.FileType('r'))
    parser.add_argument('gfa', type=argparse.FileType('w'))
    parser.add_argument("-c", "--remove-all-containment", action='store_true',
                        help="Remove all containment")
    parser.add_argument("-i", "--remove-all-internal", action='store_true',
                        help="Remove all internal match")
    parser.add_argument("-o", "--internal-match-threshold", type=float,
                        help="Set the default value of internal match",
                        default=0.8)

    arg = vars(parser.parse_args(args))

    parser = Parser(not arg["remove_all_containment"],
                    not arg["remove_all_internal"],
                    arg["internal_match_threshold"])

    for line in arg["paf"]:
        result = parser.parse_line(line)
        if result is not None:
            logging.warning(str(result))

    for line in parser.generate_gfa():
        arg["gfa"].write(line+"\n")

if __name__ == "__main__":
    main(sys.argv[1:])
