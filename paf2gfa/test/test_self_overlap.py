import pytest

import paf2gfa

def test_A_overlap_A():

    line = """
A	17472	15493	17472	-	A	17472	15493	17472	314	1979	255	cm:i:38
"""

    resu = """
H	VN:Z:1.0
"""

    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert resu.strip() == p.get_gfa().strip()
