import pytest

import paf2gfa

# overlap at A 3' same orientation
# ---------------->
#              ------------->
def test_A_3_same():
    line = "1\t1000\t20\t1000\t+\t2\t1000\t0\t980\t30\t980\t255"
    resu = "H\tVN:Z:1.0\nS\t1\t*\tLN:i:1000\nS\t2\t*\tLN:i:1000\nL\t1\t+\t2\t+\t980M\tNM:i:950\tom:f:0.00\n"

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# overlap at A 3' orientation is different 
# ---------------->          
#             <-------------                                                                             
def test_A_3_diff():                                                                             
    line = "1\t1000\t10\t1000\t-\t2\t1000\t10\t1000\t30\t980\t255" 
    resu = "H\tVN:Z:1.0\nS\t1\t*\tLN:i:1000\nS\t2\t*\tLN:i:1000\nL\t1\t+\t2\t-\t980M\tNM:i:950\tom:f:0.00\n"

    p = paf2gfa.Parser()
    p.parse_line(line) 
    
    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# overlap at A 5' orientation is same
#             -------------> 
# ---------------->
def test_A_5_same():
    line = "1\t1000\t0\t980\t+\t2\t1000\t20\t1000\t30\t980\t255"
    resu = "H\tVN:Z:1.0\nS\t1\t*\tLN:i:1000\nS\t2\t*\tLN:i:1000\nL\t2\t+\t1\t+\t980M\tNM:i:950\tom:f:0.00\n"

    p = paf2gfa.Parser() 
    p.parse_line(line) 
    
    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# overlap at A 5' orientation is different
#             ------------->
# <----------------         
def test_A_5_diff():
    line = "1\t1000\t0\t980\t-\t2\t1000\t0\t980\t30\t960\t255"
    resu = "H\tVN:Z:1.0\nS\t1\t*\tLN:i:1000\nS\t2\t*\tLN:i:1000\nL\t1\t-\t2\t+\t960M\tNM:i:930\tom:f:0.00\n"

    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))
