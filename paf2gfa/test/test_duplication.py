import pytest

import paf2gfa

# B is contain in A
# --------------------------->
#        ---------> 
def test_duplication():
    line = """
A\t1000\t200\t1000\t+\tB\t1000\t0\t800\t800\t800\t255
A\t1000\t200\t1000\t+\tB\t1000\t0\t800\t800\t800\t255
B\t1000\t0\t800\t+\tA\t1000\t200\t1000\t800\t800\t255
"""
    resu = """
H\tVN:Z:1.0
S\tA\t*\tLN:i:1000
S\tB\t*\tLN:i:1000
L\tA\t+\tB\t+\t800M\tNM:i:0\tom:f:0.00
"""

    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))
