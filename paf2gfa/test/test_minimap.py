import pytest

import paf2gfa

def test_minimap_3contain():
    line = """56001	3693	79	3329	+	63107	23535	5870	9145	350	3275	255	cm:i:38
59727	27255	14393	26854	-	63107	23535	11119	23497	1298	12461	255	cm:i:145
59847	18269	51	18268	-	63107	23535	4047	22282	2124	18235	255	cm:i:247"""

    resu = """
H	VN:Z:1.0
S	56001	*	LN:i:3693
S	63107	*	LN:i:23535
S	59727	*	LN:i:27255
S	59847	*	LN:i:18269
L	59727	+	63107	-	12461M	NM:i:11163	om:f:0.04
C	63107	+	56001	+	5870	3275M
C	63107	+	59847	-	4047	18235M
"""
    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

