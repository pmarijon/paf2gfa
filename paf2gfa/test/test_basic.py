import pytest

import paf2gfa

def test_basic():
    line = """
1   10000   2000    10000   - 2   10000   1999    10000   8001    8001    255
1   10000   0	8000	-   5	10000	1   8000    7999    7999    255
2   10000   1   8000    -   3	10000	0   8000    8000    8000    255
3   10000   2000    10000   +	4   10000 0    8000   8000    8000    255
"""

    resu = """
H	VN:Z:1.0
S	1	*	LN:i:10000
S	2	*	LN:i:10000
S	5	*	LN:i:10000
S	3	*	LN:i:10000
S	4	*	LN:i:10000
L	1	+	2	-	8001M	NM:i:0	om:f:0.00
L	1	-	5	+	7999M	NM:i:0	om:f:0.00
L	2	-	3	+	8000M	NM:i:0	om:f:0.00
L	3	+	4	+	8000M	NM:i:0	om:f:0.00
"""

    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n"))  == set(p.get_gfa().strip().split("\n"))

def test_with_2_repetition_strand_diff():
    line = """
1   10000   2000    10000   - 2   10000   1999    10000   8001    8001    255
5   10000   1	8000	-   1	10000	0   8000    8000    8000    255
2   10000   1   8000    -   3	10000	0   8000    8000    8000    255
3   10000   2000    10000   +	4   10000 0    8000   8000    8000    255
4   10000   2000    10000   +   9   10000   0   8000    8000    8000    255
4   10000   2001    10000   -   7   10000   2000   10000    8000    8000    255
8   10000   2000    10000   +   7   10000   0   8000    8000    8000    255
7   10000   2000    10000   +   6   10000   0   8000    8000    8000    255
6   10000   2001    10000   -   5   10000   2000    10000   8000    8000    255
"""

    resu = """
H	VN:Z:1.0
S	1	*	LN:i:10000
S	2	*	LN:i:10000
S	5	*	LN:i:10000
S	3	*	LN:i:10000
S	4	*	LN:i:10000
S	9	*	LN:i:10000
S	7	*	LN:i:10000
S	8	*	LN:i:10000
S	6	*	LN:i:10000
L	1	+	2	-	8001M	NM:i:0	om:f:0.00
L	5	-	1	+	8000M	NM:i:0	om:f:0.00
L	2	-	3	+	8000M	NM:i:0	om:f:0.00
L	3	+	4	+	8000M	NM:i:0	om:f:0.00
L	4	+	9	+	8000M	NM:i:0	om:f:0.00
L	4	+	7	-	8000M	NM:i:0	om:f:0.00
L	8	+	7	+	8000M	NM:i:0	om:f:0.00
L	7	+	6	+	8000M	NM:i:0	om:f:0.00
L	6	+	5	-	8000M	NM:i:0	om:f:0.00
"""
    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    print(resu)
    print()
    print(p.get_gfa())

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))
