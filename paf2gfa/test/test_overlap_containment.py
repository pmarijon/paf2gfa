import pytest

import paf2gfa

def test_A_overlap_B_B_contained_C_keep_all():

    line = """
B	8853	7897	8500	-	A	25804	24891	25773	150	882	255	cm:i:16
B	8853	5997	8553	-	C	2962	80	2901	511	2821	255	cm:i:76
"""

    resu = """
H	VN:Z:1.0
S	B	*	LN:i:8853
S	A	*	LN:i:25804
S	C	*	LN:i:2962
L	B	+	A	-	882M	NM:i:732	om:f:0.44
C	B	+	C	-	5997	2821M
"""
    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))


def test_A_overlap_B_B_contained_C_leave_all():

    line = """
B	8853	7897	8500	-	A	25804	24891	25773	150	882	255	cm:i:16
B	8853	5997	8553	-	C	2962	80	2901	511	2821	255	cm:i:76
"""

    resu = """
H	VN:Z:1.0
S	B	*	LN:i:8853
S	A	*	LN:i:25804
L	B	+	A	-	882M	NM:i:732	om:f:0.44
"""
    p = paf2gfa.Parser(False, False)
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))
