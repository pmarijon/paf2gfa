import pytest

import paf2gfa

# B is contain in A
# --------------------------->
#        ---------> 
def test_A_contain_B_keep_contain_same():
    line = "1\t2000\t500\t1500\t+\t2\t1000\t0\t1000\t30\t1000\t255"
    resu = """
H	VN:Z:1.0
S	1	*	LN:i:2000
S	2	*	LN:i:1000
C	1	+	2	+	500	1000M
"""

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# B is contain in A
# --------------------------->
#        <---------
def test_A_contain_B_keep_contain_diff():
    line = "1\t2000\t500\t1500\t-\t2\t1000\t0\t1000\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\nS\t1\t*\tLN:i:2000\nS\t2\t*\tLN:i:1000\nC\t1\t+\t2\t-\t500\t1000M\n"

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A is contain in B
#        --------->
# --------------------------->
def test_A_contained_B_keep_contain_same():
    line = "2\t1000\t0\t1000\t+\t1\t2000\t500\t1500\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\nS\t2\t*\tLN:i:1000\nS\t1\t*\tLN:i:2000\nC\t1\t+\t2\t+\t500\t1000M\n"

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A is contain in B
#        --------->
# <---------------------------
def test_A_contained_B_keep_contain_diff():
    line = "2\t1000\t0\t1000\t-\t1\t2000\t500\t1500\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\nS\t2\t*\tLN:i:1000\nS\t1\t*\tLN:i:2000\nC\t1\t+\t2\t-\t500\t1000M\n"

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# B is contain in A
# --------------------------->
#        ---------> 
def test_A_contain_B_leave_contain_same():
    line = "1\t2000\t500\t1500\t+\t2\t1000\t0\t1000\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\n"

    p = paf2gfa.Parser(False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# B is contain in A
# --------------------------->
#        <---------
def test_A_contain_B_leave_contain_diff():
    line = "1\t2000\t500\t1500\t-\t2\t1000\t0\t1000\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\n"

    p = paf2gfa.Parser(False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A is contain in B
#        --------->
# --------------------------->
def test_A_contained_B_leave_contain_same():
    line = "2\t1000\t0\t1000\t+\t1\t2000\t500\t1500\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\n"

    p = paf2gfa.Parser(False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A is contain in B
#        --------->
# <---------------------------
def test_A_contained_B_leave_contain_diff():
    line = "2\t1000\t0\t1000\t-\t1\t2000\t500\t1500\t30\t1000\t255"
    resu = "H\tVN:Z:1.0\n"

    p = paf2gfa.Parser(False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A contain B
# B contain A
# ------------------------->
# <-------------------------
def test_A_contain_B_B_contain_A():
    line = """
A\t1000\t0\t1000\t+\tB\t1000\t1\t999\t0\t1000\t255
B\t1000\t0\t1000\t+\tA\t1000\t1\t999\t0\t1000\t255
"""
    resu = """
H\tVN:Z:1.0
S\tA\t*\tLN:i:1000
S\tB\t*\tLN:i:1000
C\tB\t+\tA\t+\t1\t1000M
C\tA\t+\tB\t+\t1\t1000M
"""

    p = paf2gfa.Parser()
    p.parse_lines(line.split("\n"))

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))
    
