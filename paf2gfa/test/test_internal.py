import pytest

import paf2gfa

# A and B internal match same strand
#  --         -->
#    \       /
#     -------
#     -------
#    /       \
#  --         -->
def test_A_B_internal_match_keep_same():
    line = "A\t10000\t200\t5100\t+\tB\t10000\t100\t5000\t30\t4900\t255"

    resu = """
H\tVN:Z:1.0
S\tA\t*\tLN:i:10000
S\tB\t*\tLN:i:10000
L\tA\t+\tB\t+\t4900M\tNM:i:4870\tom:f:1.02
"""

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A and B internal match diff strand
#  --         -->
#    \       /
#     -------
#     -------
#    /       \
# <--         --
def test_A_B_internal_match_keep_diff():
    line = "A\t10000\t2200\t7100\t-\tB\t10000\t2100\t7000\t30\t4900\t255"

    resu = """
H\tVN:Z:1.0
S\tA\t*\tLN:i:10000
S\tB\t*\tLN:i:10000
L\tA\t+\tB\t-\t4900M\tNM:i:4870\tom:f:0.88
"""

    p = paf2gfa.Parser()
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A and B internal match same strand
#  --         -->
#    \       /
#     -------
#     -------
#    /       \
#  --         -->
def test_A_B_internal_match_remove_same():
    line = "A\t10000\t200\t5100\t+\tB\t10000\t100\t5000\t30\t4900\t255"

    resu = """
H\tVN:Z:1.0
"""

    p = paf2gfa.Parser(internal=False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

# A and B internal match diff strand
#  --         -->
#    \       /
#     -------
#     -------
#    /       \
# <--         --
def test_A_B_internal_match_remove_diff():
    line = "A\t10000\t2200\t7100\t-\tB\t10000\t2100\t7000\t30\t4900\t255"

    resu = """
H\tVN:Z:1.0
"""

    p = paf2gfa.Parser(internal=False)
    p.parse_line(line)

    assert set(resu.strip().split("\n")) == set(p.get_gfa().strip().split("\n"))

