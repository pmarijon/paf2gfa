# paf2gfa

Convert PAF (Pairwise Alignement Format) in [GFA1](https://github.com/GFA-spec/GFA-spec/blob/master/GFA1.md) (Graphical Fragment Assembly)

## Requirement

- networkx

## Instalation

```
pip install git+https://gitlab.inria.fr/pmarijon/paf2gfa.git
```

## Usage in cli

```
paf2gfa --help
usage: paf2gfa [-h] [-c] [-i] [-p] paf gfa

positional arguments:
  paf
  gfa

optional arguments:
  -h, --help            show this help message and exit
  -c, --remove-all-containment
                        Remove all containment (default: False)
  -i, --remove-all-internal
                        Remove all internal match (default: False)
  -o INTERNAL_MATCH_THRESHOLD, --internal-match-threshold INTERNAL_MATCH_THRESHOLD
                        Set the default value of internal match (default: 0.8)
```

## Definition

This definition are inspired by [Miniasm section 2.4.2](https://academic.oup.com/bioinformatics/article/32/14/2103/1742895#95425019)

### Containment

```
A: ------------>
B:      ---->
```

Read B are contained in read A (the container) if length of B are minus than length A and begin of B upper than begin of A and end of B minus than end of A.

### Internal match

```
                                     overhang A
A: -----------                      -------->
              \       mapping      /
               --------------------
               --------------------
              /                    \
B:      ------                      ------------------->
    overhang B
```

Overhang is the sum of minus length for each read around match zone.

If ratio overhang by mapping length is upper than 0.8 (default value) this overlap are an internal match.

## Usage as python module

```python

import paf2gfa

########
# Parse PAF
########
p = paf2gfa.Parser()
# or to ignore containment read and overlap
p = paf2gfa.Parser(containment=False)
# or to ignore internal match overlap
p = paf2gfa.Parser(internal=False)
# or to ignore internal match and containment
p = paf2gfa.Parser(False, False)

with open("my.gfa") as fh:
    p.parse_lines(fh) # Parser.parse_lines can parse any iterable
    # alternative usage
    for line in fh:
        p.parse_line(line)
        # or
        warning = p.parse_line(line)
        if warning is not None:
            print(str(warning))

# another alternative usage
def paf_generator():
    # do some cool thing
    yield line

p.parse_lines(paf_generator()):

########
# Create GFA
########

gfa = p.get_gfa()

# or

for gfa_line in p.generate_gfa():
    # do some cool thing

########
# Acces to internal object
########
p.graph # get an networkx graph where overlap are store
```

## Bug report

For bug report you can send e-mail to pierre.marijon@inria.fr or create [issue](https://gitlab.inria.fr/pmarijon/paf2gfa/issues)
