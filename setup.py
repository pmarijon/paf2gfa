#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

#from paf2gfa import __version__, __name__

try:
    from pip._internal.req import parse_requirements
except ImportError:
    from pip.req import parse_requirements

setup(
    name= "paf2gfa",
    version= "0.1",
    packages = find_packages(),

    author="Pierre Marijon",
    author_email="pierre.marijon@inria.fr",
    description="convert Pairwise Alignement Format in Graphical Fragment Assembly",
    long_description=open('README.md').read(),
    url="https://gitlab.inria.fr/pmarijon/paf2gfa",
    
    install_requires = ["networkx>=2",],
    include_package_data=True,
    
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 2 - Pre-Alpha"
    ],

    entry_points = {
        'console_scripts': [
            'paf2gfa = paf2gfa.__main__:main'
        ]
    }
)
